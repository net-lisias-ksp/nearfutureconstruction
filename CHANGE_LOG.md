# Near Future Construction :: Change Log

* 2020-1227: 1.3.1 (Nertea) for KSP 1.11.0
	+ Fix cargo parts in 1.10.x
* 2020-1223: 1.3.0 (Nertea) for KSP 1.11.0
	+ KSP 1.11.x
	+ Fix crewed truss angled adapter attach node.
	+ Set many parts as useable in inventories/construction mode
* 2020-0803: 1.2.3 (Nertea) for KSP 1.10.1
	+ KSP 1.10.x
	+ Updated MM to 4.1.4
	+ Updated B9PartSwitch to 2.17.0
* 2020-0220: 1.2.2 (Nertea) for KSP 1.9.0
	+ KSP 1.9.x
	+ Updated MM to 4.1.3
	+ Updated B9PartSwitch to 2.13.0
	+ Updates to Russian and Chinese localizations
* 2019-1106: 1.2.1 (Nertea) for KSP 1.8.1
	+ Hotfix bad patch
* 2019-1106: 1.2.0 (Nertea) for KSP 1.8.1
	+ KSP 1.8.x
	+ Updated MM to 4.1.0
	+ Updated B9PartSwitch to 2.12.1
	+ Updated MM patch pass specifiers where needed
* 2019-1004: 1.1.4 (Nertea) for KSP 1.7.3
	+ Fixed colliders on angled octo-truss 30 and 45 degree variants
	+ Fixed screwed up texture on angled crewed octo-truss
	+ Fixed surface attach node on both angles octo-trusses
	+ Fixed surface attach node on octo-truss drone core
	+ Tuned docking parameters for linear docking ports
* 2019-0821: 1.1.3 (Nertea) for KSP 1.7.0
	+ Updated B9PartSwitch to 2.9.0
	+ Updated some part variants to use new B9PS 2.9.0 features
	+ Fixed a bug with the upper cap mesh of the Annular Truss XL
* 2019-0709: 1.1.2 (Nertea) for KSP 1.7.0
	+ Updated B9PartSwitch to 2.8.0
	+ Fixed octagonal docking port collider
* 2019-0521: 1.1.1 (Nertea) for KSP 1.7.0
	+ Fixed version specification not allowing KSP 1.7.x
	+ Updates to Russian localization (Sool3)
	+ Fixed some unlocalized strings in older parts (Sool3)
* 2019-0520: 1.1.0 (Nertea) for KSP 1.6.1
	+ Updated ModuleManager to 4.0.2
	+ Updated B9PartSwitch to 2.7.0
	+ Greyscale and specular recalibration for all trusses to be consistent with Restock (adapters will be tackled later)
	+ Revised truss lithium tanks to use reflective shader for foil as in NFP 1.1+
	+ Revised truss argon tanks to use reflective shader for internal-style argon tanks as in NFP 1.1+
	+ Revised truss liquid hydrogen tank textures to align with future CryoTanks direction
	+ Removed replacement models for Not-Rockomax Micronode and Octagonal strut (now in Restock)
	+ Propagated fixes from restock to the variants of the Cubic Octagonal Strut Micro-Truss parts
	+ Remodeled part of Octo-Girder Heavy Docking Connector to use the Restock style docking port
	+ Remodeled part of Octo-Girder Docking Connector to use the Restock style docking port
	+ Tuned textures of Octo-Girder Octagonal Docking Connector to match docking design language
	+ Fixed larger radial docking connector's docking transform being recessed into the model too far
	+ Added normal maps to spinal trusses
	+ Added normal maps to annular trusses
	+ Added normal maps to radial adapters
	+ Fixed some warnings related to B9PS 2.7.x
	+ Fixed error in CryoTanks patch for some LH2-containing truss parts
* 2019-0121: 1.0.6 (Nertea) for KSP 1.6.1
	+ New translation from Levin845! (Simplified Chinese)
* 2019-0117: 1.0.5 (Nertea) for KSP 1.6.1
	+ KSP 1.6.x
	+ Updated MM to 3.1.3
	+ Updated B9PartSwitch to 2.6.0
	+ License change for code and configs to MIT
* 2018-1105: 1.0.4 (Nertea) for KSP 1.5.1
	+ KSP 1.5.x
	+ Updated MM to 3.1.0
	+ Updated B9PartSwitch to 2.4.5
	+ Fixed a issue with some parts that would cause B9PS to throw errors in versions >2.4.0
	+ Modified the orientation of the 2.5m octo-docking port to be consistent
	+ Fixed tech tree nodes for both linear docking connectors
* 2018-0807: 1.0.3 (Nertea) for KSP 1.4.4
	+ KSP 1.4.5
	+ Updated MM to 3.0.7
	+ Updated B9PartSwitch to 2.3.3
	+ Widened KSP version specification to 1.4.99
	+ Fixed a localization typo in switchable planar struts
	+ Fixed a mesh offset on some hex truss variants
* 2018-0503: 1.0.2 (Nertea) for KSP 1.4.2
	+ KSP 1.4.3
	+ Updated B9PartSwtich to 2.3.0
		- Fixed lower stack node of S-MINI Adapter being too low
	+ Fixed lower stack node of Annular Truss Docking Connector being too low
	+ Revised exact positions of nodes on Spinal Truss pieces
	+ Fixed slight offset on Pressurized Octo-Truss Mini
	+ Fixed tiny misalignment on truss mesh of some Octo-truss variants
	+ Fixed lower node height on octo-truss octo-port
	+ Fixed tiny misalignment on hollow truss mesh of some Hex trusses
		- Fixed old window texture emissive for pressurized parts being included instead of new one
	+ Fixed misalignment of meshes on angled octo-connector models
		- Fixed misaligned mesh on replaced Rockomax Micronode
	+ Fixed localization error with Planar micro trusses
* 2018-0411: 1.0.1 (Nertea) for KSP 1.4.2
	+ Fixed localization issue with SM-1 Stack Adapter
	+ Fixed hex-truss lithium tank model offset
	+ Fixed a missing normal map
	+ Fixed top attach node position for long cubic-family strut
* 2018-0410: 1.0.0 (Nertea) for KSP 1.4.2
	+ KSP 1.4.2
	+ Final content update
	+ Updated MM to 3.0.6
	+ Updated B9PartSwitch to 2.2.1
	+ Updated MiniAVC to 1.2.0.1
	+ Reworked crewed octo-trusses to match SSPXr art
	+ Converted all parts with lights to use config-based animations
	+ Revised textures of LH2/LH2O octo-truss variants to match up with CryoTanks update
	+ Revised textures of Lithium octo-truss variants to match up with NFP update
	+ Revisions to specular/normal maps of octo-trusses
	+ New/modified parts:
	+ Replaced Cubic Octagonal Strut with new model, renamed to Cubic-Family Strut
	+ Replaced Not-Rockomax Micronode and Octagonal Strut with new models
	+ Added Cubic-Family struts in 3 sizes, can switch between cubic, triangular and planar
	+ Added adapter from Cubic/Triangular to 0.625m stack
	+ Added LFO, LF, Ox, Xenon, Mono/Ec, Ore storage to the two larger Hexa-Trusses
	+ Added LH2, LH2O, Lithium, Argon storage to the two larger Hexa-Trusses if CRP is installed
	+ Added SM-1 Stack Adapter (1.25 to 0.625m adapter)
	+ Added Grip-O-Tron Linear Docking Connector
	+ Added Grip-O-Tron XL Linear Docking Connector
	+ Corrected configuration of boiloff modules when CryoTanks is installed
* 2017-1013: 0.8.4 (Nertea) for KSP 1.3.1
	+ KSP 1.3.1
	+ Dependency update
	+ Fix 30 degree angle attach node on pressurized octo-truss
* 2017-0705: 0.8.3 (Nertea) for KSP 1.3.0
	+ Updated bundled B9PartSwitch to 1.9.0
	+ Updated MM to 2.8.1
	+ Stack symmetry on 2.5m multi-adapter can now take advantage of new B9PartSwitch functions
* 2017-0626: 0.8.2 (Nertea) for KSP 1.3.0
	+ Update B9PartSwitch to 1.8.1
	+ Added Spanish Translation courtesy of forum user fitiales
* 2017-0620: 0.8.1 (Nertea) for KSP 1.3.0
	+ Fix localization error on Octo-Truss Angled Crewed piece
	+ Fix localization error on Annular Truss (m)
* 2017-0616: 0.8.0 (Nertea) for KSP 1.3.0
	+ KSP 1.3
	+ Updated bundled MM to 2.8.0
	+ Updated bundled B9PartSwitch to 1.8.0
	+ Normalized some part internal names
	+ Improved naming of most truss parts
	+ Converted to KSP 1.3 localization string methods
	+ Small tweaks to masses and costs of many parts
* 2017-0309: 0.7.6 (Nertea) for KSP 1.2.2
	+ Fixed octo-truss and spinal docking connector snap offsets
	+ Added bottom stack nodes to 1.25m multi-adapters
* 2017-0209: 0.7.5 (Nertea) for KSP 1.2.2
	+ hotfix a collider issue
* 2017-0208: 0.7.4 (Nertea) for KSP 1.2.2
	+ Updated B9PartSwitch to 1.7.1
	+ Fixed download link in .version file
	+ Added Lithium, Argon, LqdHydrogen, LqdHydrogen/Oxidizer tanks to tank-containing trusses. Only enabled if CRP is installed
	+ Added cryo cooling abilities to tank-containing trusses. Only enabled if CRP and CryoTanks are installed
	+ Fixed extraneous subtype node definitions on crewed octo-truss parts
	+ Fixed angle of 30 degree node in angled truss piece
* 2017-0123: 0.7.3 (Nertea) for KSP 1.2.2
	+ Marked for KSP 1.2.2
	+ Updated bundled MM to 2.7.5
	+ Updated bundled B9PartSwitch to 1.5.3
* 2016-1118: 0.7.2 (Nertea) for KSP 1.2.1
	+ Marked for KSP 1.2.1
	+ Updated bundled MM to 2.7.4
	+ Modular Annular Truss Adapter's collider is now hollow
	+ Modular Annular Truss Adapter now has toggleable internal core piece
	+ Fixed Modular Annular Truss Docking Connector's missing description
	+ Fixed CTT patch re-activating deprecated part
	+ Added Modular Spinal Truss: 3.75m size category square truss
		- Regular truss: hollow, saddle, demi and solid variants. Optional core and endcaps
		- Mini truss: hollow, saddle, demi and solid variants. Optional core and endcaps
		- Micro truss: hollow, saddle, demi and solid variants. Optional core and endcaps
		- Spinal to 2.5m adapter
		- Spinal to 3.75m adapter
		- Spinal locking docking port
* 2016-1026: 0.7.1 (Nertea) for KSP 1.2
	+ Docking port lights and crew tube windows now respond correctly to Lights action group
	+ Fixed error on loading of annular truss docking port (fixes docking lights also)
* 2016-1021: 0.7.0 (Nertea) for KSP 1.2
	+ KSP 1.2
	+ Updated bundled MM
	+ Updated bundled B9PartSwitch
	+ Updated all parts with various KSP 1.2 features
	+ Octo-truss drone core functions as a KerbNet/CommNet point like the large RGU
	+ Fixed a few parts having the wrong module type
	+ Moved all docking ports to the Coupling category
	+ Soft-deprecated the Grid-O-Tron 2.5m hollow docking port
	+ Adjusted the locations of many parts in the tech tree
	+ Adjusted the price of octo-truss docking ports
	+ Adjusted the price and mass of octo-truss attach node, cost varies with different structural configurations
	+ Adjusted the price of octo-trusses, cost varies with different structural configurations
	+ Adjusted the price of octo-truss pressurized adapter to be more than the unpressurized variant
	+ Adjusted the price of hex-trusses, cost varies with different structural configurations
	+ Added a variant to the Octangular Docking Port, toggles a center crew hatch
	+ Added octo-truss angular connector in 90 degree, 45 degree and 30 degree versions
	+ Added new truss set: 5m circular
	+ Regular truss: hollow, saddle and solid variants. Optional core and endcaps
	+ XL truss: hollow, saddle and solid variants. Optional core and endcaps
	+ Mini truss: hollow, saddle and solid variants. Optional core and endcaps
	+ Micro truss: hollow, saddle and solid variants. Optional core and endcaps
	+ Short 3.75->5m adapter
	+ Long 3.75->5m adapter
	+ 5m hollow Docking port
* 2016-0626: 0.6.4 (Nertea) for KSP 1.1.3
	+ KSP 1.1.3
	+ Updated bundled CRP
	+ Updated bundled B9PartSwitch
* 2016-0527: 0.6.3 (Nertea) for KSP 1.1.2
	+ Updated bundled MM
	+ Updated bundled B9PartSwitch
	+ Adjusted the costs of some octo-girder truss tanks
	+ Fixed some CRP truss tank types being initialized if CRP was not present, causing loading problems
	+ Reduced crash tolerances of trusses with Open, Hollow and Saddle configurations
	+ Added CLS config to octo-girder Drone Core (inpassable by default)
* 2016-0522: 0.6.2 (Nertea) for KSP 1.1.2
	+ Updated bundled MM
	+ Increased the cost and mass of the Modular Octo Girder Drone core slightly
	+ Added Modular Octo-Girder Nano (1/8th size truss)
	+ Added 90 degree variant of Modular Octo-Girder attach point and two hollow variants
	+ Added LF, LFO, OX, Xe, Ore and Utility versions to 2 largest octo-girder size
* 2016-0511: 0.6.1 (Nertea) for KSP 1.1.2
	+ KSP 1.1.2
	+ Updated MM version
	+ Updated B9PartSwitch version
	+ Fixed direction of stack node on Octo-Girder Radial attach node
	+ Added Modular Hexa-Girder L1, M1, M3 and adapter piece
	+ Added new models for S-MED and S-MINI radial stack adapters
	+ First pass at docking direction marking lights for docking ports
		- TEMPORARY: Removed CRP from bundle
		- TEMPORARY: Removed truss tank variants for reworking
* 2016-0422: 0.6.0 (Nertea) for KSP 1.1
	+ No changelog provided
* 2015-1224: 0.5.5 (Nertea_01) for KSP 0.7.3
	+ KSP 1.05
	+ Updated bundled IFS
	+ Updated CRP version
	+ Added MiniAVC version checking
* 2015-0628: 0.5.4 (Nertea_01) for KSP 0.7.3
	+ Tweaked a few part thermal values
	+ Updated included ISFuelSwitch
	+ Truss docking ports allow fuel crossfeed again
* 2015-0605: 0.5.3 (Nertea_01) for KSP 0.7.3
	+ Updated included ISFuelSwitch
	+ Fixed radial stack adapters not loading due to incorrect model reference
	+ Fixed missing .dds mipmaps for many parts
	+ Fixed CLS patch again
	+ Adjusted capacity of LH2/Ox and LH2 truss tanks
* 2015-0504: 0.5.2 (Nertea_01) for KSP 0.7.3
	+ CTT 2.0 bugfix
	+ Fixed mass of half-sized Octo-Truss
	+ Increased mass of Octo-Girder hubs
* 2015-0503: 0.5.1 (Nertea_01) for KSP 0.7.3
	+ KSP 1.0 update
	+ Converted all textures to DDS
	+ Package now bundles InterstellarFuelSwitch and ModuleManager
	+ Optimized textures and folder structure
	+ Revised all costs, masses and capacities
	+ Fixed surface attach nodes for all truss pieces
	+ Consolidated Octo-Truss and all fuel tanks into a single part that can switch between tanks. Hollow truss remains separate.
	+ Added Octo-Truss pressurized segment
	+ Added half-size Octo-Truss with all fuel variants
	+ Added half-size hollow Octo-Truss
	+ Added quarter-size Octo-Truss (truss only)
	+ Added Octo-Truss adapter
	+ Added pressurized Octo-Truss-2.5m adapter
	+ Added pressurized Octo-Truss crew hub
	+ Added Octo-Truss Heavy Docking Port (2.5m-compatible docking port)
	+ Added CTT support for all new parts, revised CTT locations for older parts
	+ Added CLS support for crew Octo-Trusses
* 2014-1222: 0.4.0 (Nertea_01) for KSP 0.7.3
	+ Updated to KSP version 0.90
	+ Moved some part categories around
	+ Added an optional MM patch to turn the Argon and Liquid Hydrogen truss tanks into LFO tanks
* 2014-1011: 0.3.0 (Nertea_01) for KSP 0.7.3
	+ Updated for KSP 0.25
	+ Changed many part cfg file names (part names are unchanged)
	+ Converted all textures to mbm for better memory usage
	+ Adjusted some colliders, will hopefully result in better stability
* 2014-0816: 0.2.3 (Nertea_01) for KSP 0.7.3
	+ Stack adapter mass now depends on number of ports they provide
	+ Moved Grip-O-Tron, Octo-Grider Docking Port, S-MINI Radial Stack Adapter, SM-2 Stack Dual Adapter, SM-3 Stack Triple Adapter, SM-4 Stack Quad Adapter to Nanolathing tech node
	+ Fixed Octo-Girder Argon Truss incorrect cost
	+ Many cost tweaks
* 2014-0731: 0.2.2 (Nertea_01) for KSP 0.7.3
	+ Improved the look and function of the Hollow Octo-Truss
	+ Added an Octo-Truss docking port and a skeletal 2.5m docking port
* 2014-0724: 0.2.1 (Nertea_01) for KSP 0.7.3
	+ SAVE BREAKING UPDATE!!
	+ Fixed S-MINI Adapter being half the size required
	+ Updated for KSP 0.24
	+ Adjusted costs of all parts
	+ Reduced memory usage of some parts
* 2014-0521: 0.1.0 (Nertea_01) for KSP 0.7.3
	+ Split from main Near Future pack
	+ Recoloured Rockomax Skeletal Structural Adapter to match other adapters
	+ Fixed stupidly large Hydrogen and Xenon storage truss textures
	+ Added Modular Octo-Girder Mission Support Segment and Fuel/Oxidizer Segment
	+ Added Rockomax Skeletal XL Structural Adapter (2.5m to 3.75m)
	+ Changed S-MINI stack adapter mass from 0.008 to 0.045
	+ Changed SE-4B Quinta-Adapter mass to 0.15 from 0.2
