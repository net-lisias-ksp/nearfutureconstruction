# Near Future Construction : UNOFFICIAL

This is a unofficial, non authorized repository for **Near Future Construction** for historical reference and troubleshooting.


## In a Hurry
* [Binaries](https://github.com/net-lisias-ksph/NearFutureConstruction/tree/CC-BY-NC-SA-4.0/Archive)
* [Sources](https://github.com/net-lisias-ksph/NearFutureConstruction/tree/CC-BY-NC-SA-4.0/Master)
* [Change Log](./CHANGE_LOG.md)


## Description

This pack contains structural components. There is a large truss set that can be useful when building bases or large spacecraft. There are also a number of skeletal-style stack adapters for mounting cluster engines or just looking cool. I also recently added some high-strength docking ports. 


## License

### For Releases <= 1.0.4

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/) and licensed under [CC BY NC SA](https://creativecommons.org/licenses/by-nc-sa/4.0/), what allows you to:

* copy and redistribute the material in any medium or format
* remix, transform, and build upon the material for any purpose, even commercially.

As long you

* give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* you don't use the material for commercial purposes.
* if you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* You don't

You are authorized to fork this repository under GitHub [ToS](https://help.github.com/articles/github-terms-of-service/) **and** the License above.

### For Releases >= 1.0.5

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/). All Right Reserved.

This repository is under the claim of the [right to backup](https://info.legalzoom.com/copyright-law-making-personal-copies-22200.html) and should be considered private:

> Copyright law permits you to make one copy of your computer software for the purpose of archiving the software in case it is damaged or lost. In order to make a copy, you must own a valid copy of the software and must destroy or transfer your backup copy if you ever decide to sell, transfer or give away your original valid copy. It is not legal to sell a backup copy of software unless you are selling it along with the original.

I grant you no rights on any artifact on this repository, unless you own yourself the right to use the software and authorizes me to keep backups for you:

> (a) Making of Additional Copy or Adaptation by Owner of Copy. -- Notwithstanding the provisions of section 106, it is not an infringement for the owner of a copy of a computer program to make or authorize the making of another copy or adaptation of that computer program provided:
> 
>> (2) that such new copy or adaptation is for archival purposes only and that all archival copies are destroyed in the event that continued possession of the computer program should cease to be rightful.

[17 USC §117(a)](https://www.law.cornell.edu/uscode/text/17/117)

By using this repository without the required pre-requisites, I hold you liable to copyright infringement.


## References

* [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/155465-17x-near-future-technologies-starting-17-era-updates/)
	+ [imgur](https://imgur.com/a/PmTTebZ)
	+ [CurseForge](https://www.curseforge.com/kerbal/ksp-mods/near-future-construction)
	+ [SpaceDock](https://spacedock.info/mod/563/Near%20Future%20Construction)
	+ [GitHub](https://github.com/ChrisAdderley/NearFutureConstruction)
